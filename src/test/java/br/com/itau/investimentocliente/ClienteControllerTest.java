package br.com.itau.investimentocliente;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.itau.investimentocliente.models.Cliente;
import br.com.itau.investimentocliente.services.ClienteService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ClienteControllerTest {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	ClienteService clienteService;
	
	@Test
	public void buscarClientes() throws Exception {
		Cliente cliente = new Cliente();
		cliente.setCpf("123.123.123-12");
		
		when(clienteService.buscar("123.123.123-12")).thenReturn(Optional.of(cliente));
		
		mockMvc.perform(get("/123.123.123-12"))
			.andExpect(content().string(containsString("123.123.123-12")));
	}
}
